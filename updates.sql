-- COMP3311 12s1 Assignment 3
--
-- updates.sql: changes to supplied database
-- 
-- Written by <<YOUR NAME>>, October 2013
--
-- Must include all of the SQL/PLpgSQL infrastructure that you
--   have added to the supplied database to support your PHP code
-- This must be complete (i.e. contain all required additions)
--   and must load in a single pass (i.e. no forward references)
--   and must be syntax-error free (i.e. tested before submission)

--I'm using a modified version of my old pgplsql function 
--from assignment 2 
--to get all of the members of an acad_obj_group 
--instead of writing a php version
create type AcObjRecord as (
	objtype	text,
	object	text
);

create type AcPatternType as (
	cond	text,
	rest	text,
	excl	bool
);

create or replace function parse_acad_group_patterns(definition text)
	returns setOf AcPatternType
	language plpgsql
as $$
declare
	ret 		AcPatternType;
	patterns	text[];
	pattern		text;
	comPattern	text;
	subpatterns	text[];
begin
	patterns = regexp_split_to_array(definition,',');
	
	foreach 
		pattern in array patterns
	loop
		ret.cond = null;
		ret.excl = false;
		ret.rest = '';
		
		if(ascii(comPattern) = ascii('!')) then
			--pattern is an exclusion, set exclusion bit
			ret.excl = TRUE;
		else
			ret.excl = FALSE;
		end if;
		comPattern = trim(leading '!' from pattern);

		if(UPPER(comPattern) similar to '(FREE|GEN#|GENG|ZGEN|ALL|####)%') then
			--pattern is a special case pattern
			if(comPattern similar to '%/%') then
				--pattern contains a restriction
				subpatterns = regexp_split_to_array(pattern, '/');
				if(subpatterns[1] similar to '%F=%') then
					--subpattern 1 contains the type restriction
					ret.rest = split_part(subpatterns[1],'=',2);
					ret.cond = subpatterns[2];
				elsif(subpatterns[2] similar to '%F=%') then
					--subpattern 2 contains the type restriction
					ret.rest = split_part(subpatterns[2],'=',2);
					ret.cond = subpatterns[1];
				end if;
			else
				--pattern has no faculty restruction
				ret.cond = pattern;
				ret.rest = '';
			end if;
		else
			comPattern = replace(pattern, '#', '_');

			if(comPattern similar to '%/%') then
				--pattern contains a restriction
				subpatterns = regexp_split_to_array(comPattern, '/');
				if(subpatterns[1] similar to '%F=%') then
					--subpattern 1 contains the type restriction
					ret.rest = split_part(subpatterns[1],'=',2);
					ret.cond = subpatterns[2];
				elsif(subpatterns[2] similar to '%F=%') then
					--subpattern 2 contains the type restriction
					ret.rest = split_part(subpatterns[2],'=',2);
					ret.cond = subpatterns[1];
				end if;
			else
				--pattern is an unrestricted matching pattern
				ret.cond = comPattern;
			end if;
		end if;
		return next ret;
	end loop;
	
	return;
end
$$
;

CREATE OR REPLACE FUNCTION get_acad_group_members(_sid integer)
 RETURNS SETOF acobjrecord
 LANGUAGE plpgsql
AS $$
declare
        --declare things here
	incString	text := '';
	excString	text := '';
	pattern		AcPatternType;
	definition	text;
	gdefby		text;
	gtype		text;
	queryString 	text :='';
	ignorePatterns	text;
	rec 		AcObjRecord;
	incWritten	bool := false;
begin
        --define constants
	--TODO should probably pull this constant out into a public function or something
        ignorePatterns := '(FREE|GEN#|GENG|ZGEN|ALL|####)%';
        --do things
        select a.definition, a.gdefby, a.gtype into definition, gdefby, gtype
        from acad_object_groups a
        where id = _sid;
	if(gdefby ilike 'pattern') then
		--TODO group gtype consideration
		for 
			pattern in select * from parse_acad_group_patterns(definition)
		loop
			if(UPPER(pattern.cond) similar to ignorePatterns and not pattern.excl) then
				--the pattern is one of the ignored patterns
				--assuming that for negations (!) on the 
				--special patterns they function as normal patterns
				rec.objtype := 'subject';
				--TODO for some reason crashes here
				rec.object := replace(pattern.cond,'_','#');
				return next rec;
			else
				if(pattern.excl) then
					--pattern is an exclusion
					if(char_length(excString) > 2) then
						excString := excString||' or ';
					end if;
					excString := excString||'(s.code similar to '''||pattern.cond;
					if(char_length(pattern.rest)) then
						excString := excString||''' and o.unswid = '''||pattern.rest;
					end if;
					excString := excString||''')';
				else
					--pattern is an inclusion
					if(char_length(incString) > 2) then
						incString := incString||' or ';
					end if;
					incString := incString||'(s.code similar to '''||pattern.cond;
					if(char_length(pattern.rest)) then
						incString := incString||''' and o.unswid = '''||pattern.rest;
					end if;
					incString := incString||''')';
				end if;
			end if;
		end loop;
		--at this point i have a complete inclusion and exclusion string
		queryString := 'select ''subject''::text as objtype,s.code::text as object from (subjects s join orgunits o on s.offeredby=o.id) where';
		if (char_length(incString) > 0) then
			queryString = queryString||' ('||incString||') ';
			incWritten = true;
		end if;
		
		if(char_length(excString) > 0 and incWritten) then
			queryString = queryString||'and not ('||excString||')';
		end if;
		
		if(incWritten) then
			--notice that if there are no inclusions, then negation will not do anything, 
			--so we end up with a query that returns nothing anyway.
			Raise notice 'query string is %', queryString;
			Raise notice 'inc string is %', incString;
			Raise notice 'exc is %', excString;
			return QUERY EXECUTE queryString;
		end if;
		
	elsif(gdefby ilike 'query') then
		--TODO group gtype consideration
		return QUERY
			EXECUTE 'select '''||gtype||'''::text,code::text from ('||definition||') as d';
			
		
	elsif(gdefby ilike 'enumerated') then
		--TODO group gtype consideration
		return query select ''''||gtype||''''::text, code::text from regexp_split_to_table(definition, '[,\{\};]') as code;
	end if;
	return;
end
$$
;

create or replace view subj_fac (subject, faculty)
as
	select so.subject, o.unswid from
			(select s.code as subject, facultyOf(o.id) as id from
					subjects as s
				join
					orgunits as o
				on
					(s.offeredby = o.id)) as so
		join
			orgunits as o
		on
			(so.id = o.id)
		;
-- NOTE for some reason this query hangs, but the above works just fine.
--	select s.code, o.unswid from
--			subjects as s
--		join
--			orgunits as o
--		on
--			(facultyOf(s.offeredby) = o.id)
--	;

drop view student_subject;
