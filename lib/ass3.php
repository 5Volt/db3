<?php
// COMP3311 13s2 Assignment 3
// Functions for assignment Tasks A-E
// Written by <<YOUR NAME>>, October 2013

// assumes that defs.php has already been included


// Task A: get members of an academic object group

// E.g. list($type,$codes) = membersOf($db, 111899)
// Inputs:
//  $db = open database handle
//  $groupID = acad_object_group.id value
// Outputs:
//  array(GroupType,array(Codes...))
//  GroupType = "subject"|"stream"|"program"
//  Codes = acad object codes in alphabetical order
//  e.g. array("subject",array("COMP2041","COMP2911"))

function membersOf($db,$groupID)
{
	$q = "select * from get_acad_group_members(%d) order by object";
	$grp = dbAllTuples($db, mkSQL($q, $groupID));

	foreach($grp as $tuple){
		$courses[] = $tuple["object"];
	}
	return array($grp[0]["objtype"], $courses); 
}


// Task B: check if given object is in a group

// E.g. if (inGroup($db, "COMP3311", 111938)) ...
// Inputs:
//  $db = open database handle
//  $code = code for acad object (program,stream,subject)
//  $groupID = acad_object_group.id value
// Outputs:
//  true/false
function specialMatch($db, $pattern, $subject){

	$query = "select * from subj_fac where ";
	$num = str_replace("#","_",substr($pattern["cond"],4,4));
	//deal with each possible pattern
	//TODO process the second half of patterns
	$condition = strtoupper($pattern["cond"]);
	if(preg_match("(ALL)",$condition))
		$query = $query."(not subject ilike 'GEN%')";
	elseif(preg_match("^(FREE|####)^", $condition))
		$query = $query."(not subject ilike 'GEN_') and (subject ilike '____$num')";
	elseif(preg_match("^(GEN[G#]^)", $condition))
		$query = $query."(subject ilike 'GEN_$num')";
	elseif(preg_match("^(GEN[A-FH-Z])^", $condition))
		$query = $query."(subject ilike '".substr($condition,0,4)."$num')";
	else
		return false;
	
	if(strlen($pattern["rest"])>1)
		$query = $query." and (faculty ilike '".$pattern["rest"]."')";
	
	$query = $query."and (subject ilike '$subject')";

	$result = dbOneTuple($db, $query);
	return !empty($result);
	
}

function inGroup($db, $code, $groupID)
{	

	$definition = dboneValue($db, "select definition from acad_object_groups where id = ".$groupID);
	//regex to check for free gen and all
	$specialMatches = "((FREE|####|GENG|GEN#|GEN[A-Z]|ZGEN)[1-4\#]{4}|ALL)";
	
	if(preg_match($specialMatches, strtoupper($definition))){
		//this definition contains a special match, parse it out
		$patterns = dbAllTuples($db, mkSQL("select * from parse_acad_group_patterns(%s)", $definition));
		//now that we have all the patterns, find the special ones;
		foreach($patterns as $pattern){
			if(preg_match($specialMatches, $pattern["cond"])){
				//pattern is a special pattern
				if(specialMatch($db, $pattern, $code)){
					//TODO special pattern heirachy?
					return $pattern["excl"] == "f";
				}
			}
		}
	}
	//we now have all available patterns, check if any are free/gen
	
	//if there is no free, gen or all pattern in the group definition
	//process it using the usual rules
	$q = "select * from get_acad_group_members(%d) where object = %s";
	return count(dbAllTuples($db, mkSQL($q, $groupID, $code))) > 0; 
}


// Task C: can a subject be used to satisfy a rule

// E.g. if (canSatisfy($db, "COMP3311", 2449, $enr)) ...
// Inputs:
//  $db = open database handle
//  $code = code for acad object (program,stream,subject)
//  $ruleID = rules.id value
//  $enrolment = array(ProgramID,array(StreamIDs...))
// Outputs:
function validObjType($ruleTuple, $code){
	switch($ruleTuple["type"]){
		case "CC":
		case "FE":
		case "GE":
		case "LR":
		case "MR":
		case "PE":
		case "RC":
		case "WM":
			if(!preg_match("/^[A-Z]{4}[0-9]{4}$/", $code))
				return false;
		break;
		case "DS":
			if(!preg_match('/^[A-Z]{5}[A-Z0-9]$/',$code))
				return false;
		break;
		default:
		break;
	}
	//RQ rules
	//IR rules
	return true;
}
function canSatisfy($db, $code, $ruleID, $enrolment)
{
	$ruleTuple = dbOneTuple($db, "select * from rules where id = $ruleID");
	//check that the code is for the valid object type
	if(!validObjType($ruleTuple, $code))
		return false;
	
	$groupID = $ruleTuple["ao_group"];
	$ruleType = $ruleTuple["type"];
	if(inGroup($db, $code, $groupID)){
		//TODO MR rules
		if(preg_match("/^(CC|DS|LR|PE|RC|RQ)$/", $ruleType)){
			return true;
		}

		if(preg_match("/^GEN/", $code)){
			$subjFac = dbOneValue($db, "select facultyOf(offeredby) from subjects where code ilike '$code'");
			$progFac = dbOneValue($db, mkSQL("select facultyOf(offeredby) from programs where id = %d",$enrolment[0]));
			$streamFacs = array();
			for($x = 0; $x < count($enrolment[1]); $x++){
				$streamFacs[$x] = dbOneValue($db,mkSQL("select facultyOf(offeredby) from streams where id = %d", $enrolment[1][$x]));
			}
			if(preg_match("/^GE$/", $ruleType)){
				if($subjFac == $progFac){
					return false;
				}
				foreach($streamFacs as $streamFac){
					if($streamFac == $subjFac){
						return false;
					}
				}
				return true;
			}
		} elseif(preg_match("/^FE$/", $ruleType)){
			return true;
		}
	}
	
	if(preg_match("/^(IR|WM)$/", $ruleType)){
		//TODO return false if no uoc for subject?
		return true;
	}
	
	return false;
}
// Task D: determine student progress through a degree

// E.g. $vtrans = progress($db, 3012345, "05s1");
// Inputs:
//  $db = open database handle
//  $stuID = People.unswid value (i.e. unsw student id)
//  $semester = code for semester (e.g. "09s2")
// Outputs:
//  Virtual transcript array (see spec for details)

function progress($db, $stuID, $term)
{
	return array(); // stub
}


// Task E:

// E.g. $advice = advice($db, 3012345, 162, 164)
// Inputs:
//  $db = open database handle
//  $studentID = People.unswid value (i.e. unsw student id)
//  $currTermID = code for current semester (e.g. "09s2")
//  $nextTermID = code for next semester (e.g. "10s1")
// Outputs:
//  Advice array (see spec for details)

function advice($db, $studentID, $currTermID, $nextTermID)
{
	return array(); // stub
}
?>
